package com.epam.collections.lines_processor.figures;

import java.util.*;

/**
 * Class that stores lines data, represents by equation y = kx + b
 */
public class Line {

    public Line(Point point1, Point point2) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public Set<Point> getPoints() {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public double getK() {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public double getB() {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public void setPoints(Set<Point> points) {
        throw new UnsupportedOperationException("You need to implement this method");
    }
}
