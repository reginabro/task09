package com.epam.collections.lines_processor.figures;

/**
 * Class that stores points data
 */
public class Point {

    public Point(int x, int y) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public int getX() {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public int getY() {
        throw new UnsupportedOperationException("You need to implement this method");
    }

}
